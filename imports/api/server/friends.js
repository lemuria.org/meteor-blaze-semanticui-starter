import { Meteor } from 'meteor/meteor';
import { Friends } from '../friends.js';

Meteor.publish('friends.all', function(){
	return Friends.find();
})
