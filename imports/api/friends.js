
export const Friends = new Mongo.Collection('friends');
Friends.deny({ insert() { return true; }, update() { return true; }, remove() { return true; } }); // access to collections only through method calls


/* the Grapher way that I don't yet fully understand: 
Meteor.methods({
	friends() {
		const query = Friends.createQuery({
			$options: {
				sort: { name: 1 }
			}
			name: 1,
			email: 1,
			birthday: 1
		});

		return query.fetch();
	}
})
*/