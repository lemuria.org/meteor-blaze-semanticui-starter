import { Template } from 'meteor/templating';
import './default.html';

// As example all available Template's callbacks
// layout should be considered as a regular template
Template.default.onCreated(function () { /* ... */ });
Template.default.onRendered(function () { /* ... */ });
Template.default.onDestroyed(function () { /* ... */ });
Template.default.helpers({ /* ... */ });
Template.default.events({ /* ... */ });
