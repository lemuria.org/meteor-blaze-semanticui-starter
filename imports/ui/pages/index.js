import { Template } from 'meteor/templating';
import './index.html';

// As example all available Template's callbacks
Template.index.onCreated(function () { /* ... */ });
Template.index.onRendered(function () { /* ... */ });
Template.index.onDestroyed(function () { /* ... */ });
Template.index.helpers({ /* ... */ });
Template.index.events({ /* ... */ });
