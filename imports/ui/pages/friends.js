import { Friends } from '/imports/api/friends.js';
import './friends.html';


Template.friends.onCreated(function() {
	this.subscribe('friends.all');
});

Template.friends.helpers({
	friends: ()=>{ return Friends.find(); }
});
