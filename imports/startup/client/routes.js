import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import '/imports/ui/layouts/default.js';
import '/imports/ui/components/loading.html';

// Set up all routes in the app
FlowRouter.route('/', {
  name: 'index',
  waitOn() {
    return [
      import('/imports/ui/pages/index.js'),
      import('/imports/ui/components/header.js'),
      import('/imports/ui/components/sidebar.js'),
      import('/imports/ui/components/footer.js'),
    ];
  },
  whileWaiting() {
    this.render('default', 'loading');
  },
  action() {
    this.render('default', 'index');
  }
});

FlowRouter.route('/topics', {
  name: 'topics',
  waitOn() {
    return [
      import('/imports/ui/pages/topics.js'),
      import('/imports/ui/components/header.js'),
      import('/imports/ui/components/sidebar.js'),
      import('/imports/ui/components/footer.js'),
    ];
  },
  whileWaiting() {
    this.render('default', 'loading');
  },
  action() {
    this.render('default', 'topics', { });
  }
});

FlowRouter.route('/friends', {
  name: 'friends',
  waitOn() {
    return [
      import('/imports/ui/pages/friends.js'),
      import('/imports/ui/components/header.js'),
      import('/imports/ui/components/sidebar.js'),
      import('/imports/ui/components/footer.js'),
    ];
  },
  whileWaiting() {
    this.render('default', 'loading');
  },
  action() {
    this.render('default', 'friends');
  }
});

FlowRouter.route('/calendar', {
  name: 'calendar',
  waitOn() {
    return [
      import('/imports/ui/pages/calendar.js'),
      import('/imports/ui/components/header.js'),
      import('/imports/ui/components/sidebar.js'),
      import('/imports/ui/components/footer.js'),
    ];
  },
  data(params,qs) {
    return CalendarCollection.find();
  },
  whileWaiting() {
    this.render('default', 'loading');
  },
  action() {
    this.render('default', 'calendar', { });
  }
});

// Create 404 route (catch-all)
FlowRouter.route('*', {
  action() {
    // Show 404 error page
    this.render('notFound');
  }
});
