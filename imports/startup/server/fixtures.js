// Fill the DB with example data on startup

import { Meteor } from 'meteor/meteor';
import { Friends } from '/imports/api/friends.js';

Meteor.startup(() => {
  if (Friends.find().count() === 0) {
    const data = [
      {
        name: 'Alice',
        email: 'alice@example.com',
        birthday: new Date(1988, 08, 01),
        createdAt: new Date(),
      },
      {
        name: 'Bob',
        email: 'bob@example.com',
        birthday: new Date(1969, 11, 11),
        createdAt: new Date(),
      },
    ];

    data.forEach(friend => Friends.insert(friend));
  }
});
