# Meteor - Blaze - Semantic UI - Starter Kit

This is a slightly opinionated template or starter kit containing:

* Meteor
* the Blaze template engine
* Semantic UI
* Ostrio Flow-Router (an improved flow router package)
* Meteor accounts package with alanning:roles
* msavin:mongol for debugging


clone the repository, or follow the steps below:


## Manual Install

create project
```
meteor create my-meteor-app
cd my-meteor-app
```

remove unsafe packages
```
meteor remove autopublish insecure
```

add some essential meteor packages
```
meteor add blaze-html-templates
```

add better router
```
meteor add ostrio:flow-router-extra
```

add accounts and roles
```
meteor add accounts-password accounts-ui alanning:roles
```

add debug tools
```
meteor add msavin:mongol shell-server
```

add semantic-ui
```
meteor remove standard-minifier-css
meteor add semantic:ui semantic:ui-data juliancwirko:postcss less jquery
```

edit package.json, add:
```
{
  "devDependencies": {
    "autoprefixer": "^6.3.1"
  },
  "postcss": {
    "plugins": {
      "postcss-easy-import": {},
      "autoprefixer": {"browsers": ["last 2 versions"]}
    }
  }
}
```

and then run:
```
meteor npm install --save-dev postcss postcss-load-config postcss-easy-import
```

prepare semantic-ui config
```
mkdir client/semantic-ui
touch client/semantic-ui/custom.semantic.json
```

start meteor
```
meteor
```

Edit theme settings in custom.semantic.json to your liking. 

